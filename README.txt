INSTALL

1. Normal sites/all/module/contrib/cute_date placement and enabling.

2. Enable date display for any desired node types on the node edit page by:
   -- swapping the "Submitted by" output for cute date.
   -- create a new submitted by field for re-arranging with other CCK fields.
   
3. Visit the Date and Time configure page to customize time-window output.
   Find this at: admin/settings/date-time/formats
   Details about time tokens are as follows...


THEME SPECIFIC GOTCHA!

The D6 Zen theme starts with a node template which takes-over control of the submitted by output.
You'll need to simplify back to just this...

    <span class="submitted"><?php print $submitted; ?></span>

Rather than this...

    <span class="submitted">
      <?php
        print t('Submitted by !username on !datetime',
          array('!username' => $name, '!datetime' => $date));
      ?>
    </span>


CUSTOMIZING TIME WINDOW DISPLAYS
(This feature is not yet released.)

You can use three possible date value tokens: [interval], [start] and [end], simple dates
will not have an [end] token. The [interval] represent the difference from now until the
date in question. You can refer to the various time unit values within interval via these
modifiers...
Seconds -- [interval|s]
Minutes -- [interval|i]
Hours ---- [interval|g]
Days ----- [interval|z]
Weeks ---- [interval|w]
Years ---- [interval|y]

Note: minutes are single digit in interval tokens because they are calculated from
seconds by this module, but start/end tokens only allow two digit minutes due to normal
PHP date format limitations. The unit letters were chosen from available date format codes.
